var exec = require('cordova/exec');

exports.SHA1Digest = function(arg0, success, error) {
    exec(success, error, "xdaxcrypto", "SHA1Digest", [arg0]);
}
exports.SHA224Digest = function(arg0, success, error) {
    exec(success, error, "xdaxcrypto", "SHA224Digest", [arg0]);
}
exports.SHA256Digest = function(arg0, success, error) {
    exec(success, error, "xdaxcrypto", "SHA256Digest", [arg0]);
}
exports.SHA384Digest = function(arg0, success, error) {
    exec(success, error, "xdaxcrypto", "SHA384Digest", [arg0]);
}
exports.SHA512Digest = function(arg0, success, error) {
    exec(success, error, "xdaxcrypto", "SHA512Digest", [arg0]);
}
exports.MD2Digest = function(arg0, success, error) {
    exec(success, error, "xdaxcrypto", "MD2Digest", [arg0]);
}
exports.MD4Digest = function(arg0, success, error) {
    exec(success, error, "xdaxcrypto", "MD4Digest", [arg0]);
}
exports.MD5Digest = function(arg0, success, error) {
    exec(success, error, "xdaxcrypto", "MD5Digest", [arg0]);
}