package cordova.plugins.xdaxcrypto;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import org.bouncycastle.crypto.digests.MD2Digest;
import org.bouncycastle.crypto.digests.MD4Digest;
import org.bouncycastle.crypto.digests.MD5Digest;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.digests.SHA224Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.digests.SHA384Digest;
import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.util.encoders.Hex;
/**
 * This class echoes a string called from JavaScript.
 */
public class xdaxcrypto extends CordovaPlugin {

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {
        if(action.equals("SHA1Digest")) {
            final String input = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                public void run(){
                    if(input.length() > 0){
                        try{
                            callbackContext.success(new String(Hex.encode(SHA1Digest(input.getBytes()))));
                        }catch(Exception ex){
            				callbackContext.error(ex.toString());
                        }
                    }else {
            			callbackContext.error("Length of input is not correct.");
                    }
                }
            });
            return true;
        }else if(action.equals("SHA224Digest")) {
            final String input = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                public void run(){
                    if(input.length() > 0){
                        try{
                            callbackContext.success(new String(Hex.encode(SHA224Digest(input.getBytes()))));
                        }catch(Exception ex){
            				callbackContext.error(ex.toString());
                        }
                    }else {
            			callbackContext.error("Length of input is not correct.");
                    }
                }
            });
            return true;
        }else if(action.equals("SHA256Digest")) {
            final String input = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                public void run(){
                    if(input.length() > 0){
                        try{
                            callbackContext.success(new String(Hex.encode(SHA256Digest(input.getBytes()))));
                        }catch(Exception ex){
            				callbackContext.error(ex.toString());
                        }
                    }else {
            			callbackContext.error("Length of input is not correct.");
                    }
                }
            });
            return true;
        }else if(action.equals("SHA384Digest")) {
            final String input = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                public void run(){
                    if(input.length() > 0){
                        try{
                            callbackContext.success(new String(Hex.encode(SHA384Digest(input.getBytes()))));
                        }catch(Exception ex){
            				callbackContext.error(ex.toString());
                        }
                    }else {
            			callbackContext.error("Length of input is not correct.");
                    }
                }
            });
            return true;
        }else if(action.equals("SHA512Digest")) {
            final String input = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                public void run(){
                    if(input.length() > 0){
                        try{
                            callbackContext.success(new String(Hex.encode(SHA512Digest(input.getBytes()))));
                        }catch(Exception ex){
            				callbackContext.error(ex.toString());
                        }
                    }else {
            			callbackContext.error("Length of input is not correct.");
                    }
                }
            });
            return true;
        }else if(action.equals("MD2Digest")) {
            final String input = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                public void run(){
                    if(input.length() > 0){
                        try{
                            callbackContext.success(new String(Hex.encode(MD2Digest(input.getBytes()))));
                        }catch(Exception ex){
            				callbackContext.error(ex.toString());
                        }
                    }else {
            			callbackContext.error("Length of input is not correct.");
                    }
                }
            });
            return true;
        }else if(action.equals("MD4Digest")) {
            final String input = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                public void run(){
                    if(input.length() > 0){
                        try{
                            callbackContext.success(new String(Hex.encode(MD4Digest(input.getBytes()))));
                        }catch(Exception ex){
            				callbackContext.error(ex.toString());
                        }
                    }else {
            			callbackContext.error("Length of input is not correct.");
                    }
                }
            });
            return true;
        }else if(action.equals("MD5Digest")) {
            final String input = args.getString(0);
            cordova.getThreadPool().execute(new Runnable() {
                public void run(){
                    if(input.length() > 0){
                        try{
                            callbackContext.success(new String(Hex.encode(MD5Digest(input.getBytes()))));
                        }catch(Exception ex){
            				callbackContext.error(ex.toString());
                        }
                    }else {
            			callbackContext.error("Length of input is not correct.");
                    }
                }
            });
            return true;
        }
        return false;
    }
    public static byte[] SHA1Digest(byte[] input) {
	    SHA1Digest d = new SHA1Digest();
	    d.update(input, 0, input.length);
	    byte[] result = new byte[d.getDigestSize()];
	    d.doFinal(result, 0);
	    return result;
	}
	public static byte[] SHA224Digest(byte[] input) {
	    SHA224Digest d = new SHA224Digest();
	    d.update(input, 0, input.length);
	    byte[] result = new byte[d.getDigestSize()];
	    d.doFinal(result, 0);
	    return result;
	}
	public static byte[] SHA256Digest(byte[] input) {
	    SHA256Digest d = new SHA256Digest();
	    d.update(input, 0, input.length);
	    byte[] result = new byte[d.getDigestSize()];
	    d.doFinal(result, 0);
	    return result;
	}
	public static byte[] SHA384Digest(byte[] input) {
	    SHA384Digest d = new SHA384Digest();
	    d.update(input, 0, input.length);
	    byte[] result = new byte[d.getDigestSize()];
	    d.doFinal(result, 0);
	    return result;
	}
	public static byte[] SHA512Digest(byte[] input) {
	    SHA512Digest d = new SHA512Digest();
	    d.update(input, 0, input.length);
	    byte[] result = new byte[d.getDigestSize()];
	    d.doFinal(result, 0);
	    return result;
	}
	public static byte[] MD2Digest(byte[] input) {
	    MD2Digest d = new MD2Digest();
	    d.update(input, 0, input.length);
	    byte[] result = new byte[d.getDigestSize()];
	    d.doFinal(result, 0);
	    return result;
	}
	public static byte[] MD4Digest(byte[] input) {
	    MD4Digest d = new MD4Digest();
	    d.update(input, 0, input.length);
	    byte[] result = new byte[d.getDigestSize()];
	    d.doFinal(result, 0);
	    return result;
	}
	public static byte[] MD5Digest(byte[] input) {
	    MD5Digest d = new MD5Digest();
	    d.update(input, 0, input.length);
	    byte[] result = new byte[d.getDigestSize()];
	    d.doFinal(result, 0);
	    return result;
	}
}
