xdaxcrypto

Installation:
First clone it and then add in your project like: cordova plugin add projectpath/xdaxcrypto

Use:
	var objx = cordova.plugins.xdaxcrypto;
    objx.SHA1Digest("Danish",function(success){
      alert(success);
    },function(error){
      alert(error);
    })
	
	objx.SHA224Digest("Danish",function(success){
      alert(success);
    },function(error){
      alert(error);
    })
	
	objx.SHA256Digest("Danish",function(success){
      alert(success);
    },function(error){
      alert(error);
    })
	
	objx.SHA384Digest("Danish",function(success){
      alert(success);
    },function(error){
      alert(error);
    })
	
	objx.SHA512Digest("Danish",function(success){
      alert(success);
    },function(error){
      alert(error);
    })
	
	objx.MD2Digest("Danish",function(success){
      alert(success);
    },function(error){
      alert(error);
    })
	
	objx.MD4Digest("Danish",function(success){
      alert(success);
    },function(error){
      alert(error);
    })
	
	objx.MD5Digest("Danish",function(success){
      alert(success);
    },function(error){
      alert(error);
    })

I will implement file encryption and decryption, key generation and public key cryptography as soon. 	
If you want to add some thing new in this so kindly email me on danishashraf047@gmail.com